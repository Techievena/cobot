# Description:
#   Allows users to assign themselves to github issues across all coala repos
#
# Commands:
#   hubot assign <issue_link> - Assigns issue <issue_link> to user
#
# Configuration:
#   HUBOT_GITHUB_OAUTH
#
# Author:
#   Meet Mangukiya (@meetmangukiya)

gh_token = process.env.HUBOT_GITHUB_OAUTH

module.exports = (robot) ->
  robot.respond /assign (https:\/\/github.com\/coala\/([^\/]+)\/issues\/(\d+))/i, (msg) ->
    repo = msg.match[2]
    issueNumber = msg.match[3]
    assignee_requester = msg.message.user.login

    robot.http("https://api.github.com/repos/coala/#{repo}/issues/#{issueNumber}")
          .get() (err, res, body) ->
            if err
              console.log("Error : #{err}")
              msg.send "Error occured while checking the assignee of that issue"
            if JSON.parse(body)["assignee"] is null # Check if the issue already has an assignee
              robot.http("https://api.github.com/repos/coala/#{repo}/issues/#{issueNumber}/assignees")
                    .header('Authorization', "token #{gh_token}")
                    .post(JSON.stringify({assignees: [assignee_requester]})) (err, res, body) ->
              if res.statusCode is not 201 or err
                if err is not undefined
                  msg.send "Assigning failed :( #{err}"
                else
                  msg.send "Assigning failed :( #{res.statusCode}"
              else
                msg.send ":tada: You have been assigned to #{msg.match[1]}"
            else
              msg.send "Issue already assigned to someone, please lookout for other [issues](https://github.com/issues?utf8=%E2%9C%93&q=is%3Aopen+is%3Aissue+user%3Acoala)"
